package model
{
	import flash.display.Sprite;
	
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2Fixture;

	public class Ring
	{
		private var _body:b2Body;
		private var _bodyDef:b2BodyDef;
		private var _view:Sprite;
		
		private var _gates:Vector.<b2Fixture>;
		private var _walls:Vector.<b2Fixture>;
		
		public function Ring()
		{
			_view = new Sprite();
			_view.graphics.beginFill(0xCCCCC);
			_view.graphics.drawCircle(0,0, Ball.radiusPx * 5);
			_view.graphics.endFill();
			
			_bodyDef = new b2BodyDef();
			_bodyDef.type = b2Body.b2_staticBody;
			_bodyDef.allowSleep = false;
			
			_gates = new Vector.<b2Fixture>();
			_walls = new Vector.<b2Fixture>();
		}
		
		public function get view():Sprite
		{
			return _view;
		}

		public function get bodyDef():b2BodyDef
		{
			return _bodyDef;
		}

		public function get body():b2Body
		{
			return _body;
		}

		public function set body(value:b2Body):void
		{
			_body = value;

			var angle:Number = 0;//центральный угол
			var r:Number = 5 * Ball.radius * 2;
			var rSmall:Number = r-0.2; //меньший радиус, 0.2 - толщина стенки
			
			//находим длину дуги, достаточную чтобы пролез наш шарик
			var horda:Number = Ball.radius * 2 + 0.3; //хорда, дырка под шарик с запасом по 0.3
			//считаем так http://altera-pars.narod.ru/Qadra/duga.htm
			var ballL:Number = 2 * 2 * 10 * Math.sqrt(2 - Math.sqrt(2 + Math.sqrt(4 - Math.pow((horda/r), 2))));//длина дуги для дырки
			//теперь можем найти длину дуги для стенок
			var wallL:Number = (2 * Math.PI * r - 3 * ballL) / 3;
			
			//суть построения такова: наращиваем l-длину дуги, пока она не достигнет требуемой длины дуги, попутно вычисляем точки из которых строим полигон
			var lengths:Array = []; //соберём опорные длины дуг в кучу
			var total:Number = 0;
			for(var i:int = 0; i<6; i++)
			{
				if(i%2 == 0)
				{
					total += wallL;
				}
				else
				{
					total += ballL;
				}
				lengths.push(total);
			}
			
			//начинаем проходку
			var l:Number;//длина дуги
			var index:int = 0;
			var maxL:Number = ballL; //длина дуги, где мы ставим точку
			var currentL:Number = 0;
			var pX:Number;
			var pY:Number;
			var pXsmall:Number;
			var pYsmall:Number;
			var points:Vector.<b2Vec2>;//точки по большему радиусу
			var pointsSmall:Vector.<b2Vec2>;//точки по меньшему радиусу
			while(angle <= Math.PI)
			{
				if(l == 0)
				{
					pX = r * Math.cos(angle);
					pY = r * Math.sin(angle);
					pXsmall = rSmall * Math.cos(angle);
					pYsmall = rSmall * Math.sin(angle);
					//отмечаем первую точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
				}
				else if(l == lengths[index])
				{
					//дошли до конца сегмента
					//пришло время определить точку и выставить её
					pX = r * Math.cos(angle);
					pY = r * Math.sin(angle);
					pXsmall = rSmall * Math.cos(angle);
					pYsmall = rSmall * Math.sin(angle);
					//отмечаем точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					
					//смена сегмента дуги, теперь нам надо построить фикстуру по точкам
					//строится фикстура против часовой стрелки, т.о. мы объединяем массивы pointsSmall и points, 
					//при этом points делаем reverse()
					points = points.reverse();
					points = pointsSmall.concat(points);
					if(i % 2 == 0)
					{
						createSegmentFixture(points);
					}
					else
					{
						createGateFixture(points);
					}
					
					
					points = new Vector.<b2Vec2>();
					pointsSmall = new Vector.<b2Vec2>();
					
					//при этом в той конечной точке берёт начало уже другая фикстура
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					index++;
				}
				else if(currentL == maxL)
				{
					//пришло время определить точку и выставить её
					pX = r * Math.cos(angle);
					pY = r * Math.sin(angle);
					pXsmall = rSmall * Math.cos(angle);
					pYsmall = rSmall * Math.sin(angle);
					//отмечаем точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					
					currentL = 0;
				}

				currentL += 0.1;
				l += 0.1;
				angle = 180 * l/ (r * Math.PI); //угол
			}
		}
		
		private function createGateFixture(points:Vector.<b2Vec2>):void
		{
			//создаём фикстуру дырки, через которую будет пролезать шарик
			//фикстура для детекции пролез шарик или нет, если пролез, то столкновений с ней не будет
			//если застрял в ней, то столкновения есть и шарик надо вернуть на исходную позицию
			var s:b2PolygonShape = b2PolygonShape.AsVector(points, points.length);
			var f:b2Fixture = _body.CreateFixture2(s, 5);
			_gates.push(f);
		}
		
		private function createSegmentFixture(points:Vector.<b2Vec2>):void
		{
			//создаём полигональную форму
			var s:b2PolygonShape = b2PolygonShape.AsVector(points, points.length);
			//добавляем эту фикстуру к телу
			var f:b2Fixture = _body.CreateFixture2(s, 5);
			_walls.push(f);
		}
		
		public function update() : void
		{
			_view.x = _body.GetPosition().x * Config.BOXSCALE;
			_view.y = _body.GetPosition().y * Config.BOXSCALE;
			_view.rotation = _body.GetAngle()*180/Math.PI;
		}
	}
}