package model
{
	import flash.display.Sprite;
	
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;

	public class Ball
	{
		private static const _radius:Number = 5;
		private static var _bodyDef:b2BodyDef;

		private var _color:Number;
		private var _body:b2Body;
		private var _view:Sprite;
		
		public function get view():Sprite
		{
			return _view;
		}

		public static function get radius():Number
		{
			return _radius;
		}

		public static function get radiusPx() : Number
		{
			return _radius * Config.BOXSCALE;
		}
		
		public static function get bodyDef():b2BodyDef
		{
			_bodyDef = new b2BodyDef();
			_bodyDef.type = b2Body.b2_dynamicBody;
			_bodyDef.linearDamping = 0.3;
			_bodyDef.angularDamping = 0.1;
			_bodyDef.allowSleep = false;
			return _bodyDef;
		}
		
		public function get body():b2Body
		{
			return _body;
		}
		
		public function set body(value:b2Body):void
		{
			_body = value;
			var shape:b2CircleShape = new b2CircleShape(radius);
			_body.CreateFixture2(shape, 5);
		}
		
		public function Ball(ballColor:Number = -1)
		{
			//pick random color
			if(ballColor == -1)
			{
				var i:int = Math.random() * ColorEnum.colors.length;
				ballColor = ColorEnum.colors[i];
			}
			_color = ballColor;
			_view = new Sprite();
			_view.graphics.beginFill(_color);
			_view.graphics.drawCircle(0,0, Ball.radiusPx);
			_view.graphics.endFill();
		}
		
		public function get color():Number
		{
			return _color;
		}
		
		public function update() : void
		{
			_view.x = _body.GetPosition().x * Config.BOXSCALE;
			_view.y = _body.GetPosition().y * Config.BOXSCALE;
			_view.rotation = _body.GetAngle()*180/Math.PI;
		}
		
		public function clear():void
		{
			if(_view && _view.parent)
			{
				_view.parent.removeChild(_view);
				_view.graphics.clear();
			}
		}
	}
}