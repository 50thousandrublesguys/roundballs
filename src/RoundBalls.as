package
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	import Box2D.Collision.b2Point;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	
	import model.Ball;
	import model.Config;
	import model.Ring;
	
	public class RoundBalls extends Sprite
	{
		private var _balls:Vector.<Ball>;
		private var _ring:Ring;
		
		private var _world:b2World;
		private var _boxScale:Number;
		private var _debugSprite:Sprite;
		private var _debugDraw:b2DebugDraw;
		
		private var _ballsContainer:Sprite;
		
		public function RoundBalls()
		{
			if(stage)
			{
				init(null);				
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
			
//			_world = new b2World(new b2Vec2(0,10), true);
//			_boxes = new Vector.<b2Body>;
//			
//			var roundShape:b2PolygonShape = new b2PolygonShape();
//			
//			for (var i:int = 0; i< balls.numChildren; i++)
//			{
//				var child:Sprite = balls.getChildAt(i) as Sprite;
//				addBall(child, 1);
//			}
//			var source:Vector.<Sprite> = new Vector.<Sprite>;
//			for (var i:int = 0; i< walls.numChildren; i++)
//			{
//				var child:Sprite = walls.getChildAt(i) as Sprite;
//				_boxes.push(addBox(child, 1));
//			}
//			
//			_jointTarget = addBall(jointTarget, 1);
//			
//			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();
//			jointDef.enableLimit = true;
//			jointDef.lowerAngle = 0;
//			jointDef.upperAngle = 0;
//			for each (var body:b2Body in _boxes)
//			{
//				jointDef.Initialize(body, _jointTarget, _jointTarget.GetWorldCenter());
//				_world.CreateJoint(jointDef);
//			}
//			
//			//addToShape(source);
//			
//			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function init(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createWorld();
			createDebug();
			createRing();
			createBalls();
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);

		}
		
		private function createWorld() : void
		{
			_world = new b2World(new b2Vec2(0,0), true);
			_boxScale = Config.BOXSCALE;
		}
		
		private function createDebug():void
		{
			_debugSprite = new Sprite();
			addChild(_debugSprite);
			//создаём экземпляр
			_debugDraw = new b2DebugDraw();
			//передаём спрайт для отрисовки
			_debugDraw.SetSprite(_debugSprite);
			//передаём масштаб мира
			_debugDraw.SetDrawScale(_boxScale);
			//настраиваем прозрачность
			_debugDraw.SetFillAlpha(0.5);
			//настраиваем толщину линий
			_debugDraw.SetLineThickness(1);
			//выставляем флаги  показа информации
			_debugDraw.SetFlags(b2DebugDraw.e_shapeBit|b2DebugDraw.e_jointBit|b2DebugDraw.e_centerOfMassBit|b2DebugDraw.e_aabbBit|b2DebugDraw.e_controllerBit|b2DebugDraw.e_pairBit);
			//передаём отрисовщик миру
			_world.SetDebugDraw(_debugDraw);
		}
		
		private function createBalls() : void
		{
			_ballsContainer = new Sprite();
			addChild(_ballsContainer);
		}
		
		private function createBall() : Ball
		{
			var b:Ball = new Ball();
			b.body = _world.CreateBody(Ball.bodyDef);
			return b;
		}
		private function removeBall(ball:Ball) : void
		{
			_world.DestroyBody(ball.body);
			ball.clear();
			ball.body = null;
			ball = null;
		}
		
		
		private function createRing() : void
		{
			_ring = new Ring();
			_ring.body = _world.CreateBody(_ring.bodyDef);
		}
		
		
		protected function update(event:Event):void
		{
			_world.Step(1/60, 8, 8);
			_world.ClearForces();
			_world.DrawDebugData() 
		}
		
		private function convertToBox(value:Number) : Number
		{
			return value / _boxScale;
		}
		
//		public function onEnterFrame(e:Event = null):void 
//		{
//			_world.Step(1 / 30, 4, 4);
//			
//			for (var bb:b2Body = _world.GetBodyList(); bb; bb = bb.GetNext()){
//				if (bb.GetUserData() is Sprite) 
//				{
//					_jointTarget.SetAngle(_jointTarget.GetAngle()+0.01);
//					var sprite:Sprite = bb.GetUserData() as Sprite;
//					sprite.x = bb.GetPosition().x * PIXELS_TO_METRE 
//					sprite.y = bb.GetPosition().y * PIXELS_TO_METRE 
//					
//						//sprite.x += sprite.width/2;
//						//sprite.y += sprite.height/2;
//					
//					sprite.rotation = bb.GetAngle() * (180 / Math.PI);
//				}
//			}
//			
//		}
		
		/*public function addToShape (source:Vector.<Sprite>):b2Body
		{
			var verticies:Vector.<b2Vec2> = new Vector.<b2Vec2>;
			for (var i:int = 0; i < source.length; i++)
			{
				var el:Sprite = source[i];
				const sizeObj:DisplayObject = el.getChildAt(0);
				var point:Point = new Point(-sizeObj.width/2,- sizeObj.height/2)
					point = el.localToGlobal(point);
				verticies.push(new b2Vec2(point.x, point.y));
				var point:Point = new Point(sizeObj.width/2, - sizeObj.height/2)
					point = el.localToGlobal(point);
				verticies.push(new b2Vec2(point.x, point.y));
			}
			
			for (var j:int = source.length; j > 0; j--)
			{
				var el:Sprite = source[j];
				const sizeObj:DisplayObject = el.getChildAt(0);
				var point:Point = new Point(sizeObj.width/2,sizeObj.height/2)
				point = el.localToGlobal(point);
				verticies.push(new b2Vec2(point.x, point.y));
				var point:Point = new Point(-sizeObj.width/2, sizeObj.height/2)
				point = el.localToGlobal(point);
				verticies.push(new b2Vec2(point.x, point.y));
			}
			
			var shape:b2PolygonShape = new b2PolygonShape();
			shape.SetAsVector(verticies, verticies.length);
			
			return shape;
		}
		*/
//		public function addBox(source:Sprite, dyn:int = 0, existingShape:b2Shape = null) : b2Body 
//		{
//			const width:Number = source.getChildAt(0).width;
//			const height:Number = source.getChildAt(0).height;
//			const x:Number = source.x;
//			const y:Number = source.y;
//			const angle:Number = source.rotation;
//			var bodyDef:b2BodyDef = new b2BodyDef();
//			bodyDef.position.Set((x - 0*width/2 )/ PIXELS_TO_METRE, (y - 0*height/2) / PIXELS_TO_METRE);
//			bodyDef.angle = (angle / 180 * Math.PI);
//			
//			if (dyn == 1) bodyDef.type = b2Body.b2_dynamicBody;
//			var content:Sprite = source
//			
//			bodyDef.userData = content;
//			addChild(bodyDef.userData); 
//			var boxShape:b2PolygonShape = new b2PolygonShape();
//			boxShape.SetAsBox(width / 2 / PIXELS_TO_METRE, height / 2 / PIXELS_TO_METRE);
//			
//			var fixtureDef:b2FixtureDef = new b2FixtureDef();
//			fixtureDef.shape = existingShape ? existingShape : boxShape;
//			fixtureDef.density = dyn;
//			var body:b2Body = _world.CreateBody(bodyDef);
//			body.CreateFixture(fixtureDef);
//			//body.SetAngle(deg2rad(angle));
//			
//			return body;
//		}
//		
//		private function deg2rad(angle:Number):Number
//		{
//			return angle/(180 / Math.PI);
//		}
//		
//		public function addBall(source:Sprite, dyn:int = 0):b2Body 
//		{
//			const radius:Number = source.width/2;
//			const x:Number = source.x;
//			const y:Number = source.y;
//			
//			var bodyDef:b2BodyDef = new b2BodyDef();
//			bodyDef.position.Set((x + radius) / PIXELS_TO_METRE, (y + radius) / PIXELS_TO_METRE);
//			if (dyn == 1) bodyDef.type = b2Body.b2_dynamicBody;
//			var content:Sprite = source
//			
//			bodyDef.userData = content;
//			addChild(bodyDef.userData);
//			var circShape:b2CircleShape = new b2CircleShape(radius / PIXELS_TO_METRE);
//			var fixtureDef:b2FixtureDef = new b2FixtureDef();
//			fixtureDef.shape = circShape;
//			fixtureDef.density = dyn;
//			var ball:b2Body = _world.CreateBody(bodyDef);
//			ball.CreateFixture(fixtureDef);
//			return ball;
//		}
	}
}